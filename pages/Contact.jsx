import Head from "next/head";
import Link from "next/link";
import { motion } from "framer-motion";

export default function Contact() {
     // ANIMATIONS

     const easing = [0.6, -0.05, 0.01, 0.99] ;
     const fadeInUp = {
       initial: {
         y: 60, 
         opacity: 0
       },
       animate: {
         y: 0,
         opacity: 1,
         transition: {
           duration: 0.6,
           ease: easing,
           delay: 0.3
         }
       }
     } ;
   
  return (
    <motion.div className="App"
      animate= {{ opacity: 1, y: 0}}
      initial= {{ opacity: 0, y: -100}}
      transition= {{ duration: .6}}
      exit={{ opacity: 0, y: 300}}
    >
      <Head>
        <link
          href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
        <link rel="icon" href="../assets/favicon.jpg" />
        <link rel="stylesheet" href="css/style.css"></link>
      </Head>
      <motion.main 
      initial='initial'
      animate='animate'>
        <section className="text-gray-700 body-font">
          <header className="shadow-2xl lg:px-16 px-6 bg-white flex flex-wrap items-center lg:py-0 py-2">
            <div className="flex-1 flex justify-between items-center">
              <Link href="/">
                <a href="#">
                  <img
                    alt="logo"
                    className="w-40 mb-2"
                    src="./assets/logo.png"
                  ></img>
                </a>
              </Link>
            </div>

            <label for="menu-toggle" className="pointer-cursor lg:hidden block">
              <svg
                className="fill-current text-gray-900"
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
              >
                <title className="pointer-cursor">MENU</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
              </svg>
            </label>
            <input className="hidden" type="checkbox" id="menu-toggle" />

            <div
              className="hidden lg:flex lg:items-center lg:w-auto w-full"
              id="menu"
            >
              <nav>
                <ul className="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Product
                    </a>
                  </li>
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Pricing
                    </a>
                  </li>
                  <li>
                    <Link href="/AboutUs">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        About Us
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/Contact">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        Contact
                      </a>
                    </Link>
                  </li>
                  <li>
                    <a
                      className="font-bold lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400 lg:mb-0 mb-2"
                      href="#"
                    >
                      Sign In
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </header>
        </section>

        <section className="text-gray-700 body-font relative">
          <div class="container px-5 py-10 mx-auto">
            <div class="flex flex-col text-center w-full mb-0">
              <motion.h1 variants={fadeInUp} class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                Contact
              </motion.h1>
              <motion.p variants={fadeInUp} class="lg:w-2/3 mx-auto leading-relaxed text-base">
                We are eager to get in touch with you!
              </motion.p>
            </div>
          </div>
          <motion.div variants={fadeInUp} className="container px-5 py-10 mx-auto flex sm:flex-no-wrap flex-wrap">
            <div className="lg:w-2/3 md:w-1/2 bg-gray-300 rounded-lg overflow-hidden sm:mr-10 p-10 flex items-end justify-start relative">
              <iframe
                width="100%"
                height="100%"
                className="absolute inset-0"
                frameborder="0"
                title="map"
                marginheight="0"
                marginwidth="0"
                scrolling="no"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d243646.88006253017!2d78.26796107344876!3d17.412627412825287!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb99daeaebd2c7%3A0xae93b78392bafbc2!2sHyderabad%2C%20Telangana!5e0!3m2!1sen!2sin!4v1596483504670!5m2!1sen!2sin"
              ></iframe>
              <div className="bg-white relative flex flex-wrap py-6">
                <div className="lg:w-1/2 px-6">
                  <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm">
                    ADDRESS
                  </h2>
                  <p className="leading-relaxed">
                    Lorem Ipsum, is simply dummy text, of the printing and,
                    typesetting, 411000.
                  </p>
                </div>
                <div className="lg:w-1/2 px-6 mt-4 lg:mt-0">
                  <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm">
                    EMAIL
                  </h2>
                  <a className="text-indigo-500 leading-relaxed">
                    loremipsum@email.com
                  </a>
                  <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mt-4">
                    PHONE
                  </h2>
                  <p className="leading-relaxed">+91-1234567890</p>
                </div>
              </div>
            </div>
            <div className="lg:w-1/3 md:w-1/2 bg-white flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
              <input
                className="bg-white rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4"
                placeholder="First Name"
                type="text"
              />
              <input
                className="bg-white rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4"
                placeholder="Last Name"
                type="text"
              />
              <input
                className="bg-white rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4"
                placeholder="Email Address"
                type="email"
              />
              <textarea
                className="bg-white rounded border border-gray-400 focus:outline-none h-32 focus:border-indigo-500 text-base px-4 py-2 mb-4 resize-none"
                placeholder="Your Message"
              ></textarea>
              <button className="text-white bg-blue-500 border-0 py-2 px-6 shadow-xl focus:outline-none hover:bg-blue-600 rounded text-lg">
                Send
              </button>
            </div>
          </motion.div>
        </section>
      </motion.main>
    </motion.div>
  );
}
