import Head from "next/head";
import Link from "next/link";
import { Swipeable } from "react-touch";
import { motion, AnimatePresence } from 'framer-motion' ;
import React from 'react' ;



let i = 0;
const testimonialArray = [
  {
    src: "https://i.pinimg.com/originals/4f/05/72/4f05724331fdf3cbcced3ca99b046d06.png",
    name: "Mr./Mrs User No. 1",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://i.pinimg.com/236x/be/df/6c/bedf6cff125ecc4f134e31a0e6ed1dcf.jpg",
    name: "Mr./Mrs User No. 2",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://i.pinimg.com/originals/3d/9a/6d/3d9a6d6289cc90565968f8fd42acf27e.png",
    name: "Mr./Mrs User No. 3",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRAes2ryDnsTdZ692Anl1oFhJO4bQndj1JN-Q&usqp=CAU",
    name: "Mr./Mrs User No. 4",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://i.pinimg.com/originals/3d/d0/2d/3dd02d298c1aff37ff8b2d0c01ae5025.png",
    name: "Mr./Mrs User No. 5",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://androidfantasy.org/wp-content/uploads/2019/07/girl-with-guitar-dp.jpg",
    name: "Mr./Mrs User No. 6",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
];
export default function Home() {

  const [show, setshow] = React.useState(0) ;
  const [Yposition, setYposition] = React.useState(100) ;

  const handleScroll = (e) => {
    if(window.pageYOffset >= 500){
      setshow(1) ;
      setYposition(0) ;
    }
  }
  React.useEffect(() => {
      window.addEventListener('scroll', handleScroll);
      return () => window.removeEventListener('scroll', handleScroll);
    });

  
  const FeaturesState = React.useState(1);
  const selectedIndex = FeaturesState[0];
  const setSelectedIndex = FeaturesState[1];

  const checkNext = () => {
    const labels = document.querySelectorAll("#slider label");
    const nextIndex =
      FeaturesState[0] === labels.length - 1 ? 0 : FeaturesState[0] + 1;
    FeaturesState[1](nextIndex);
  };
  const checkPrev = () => {
    const labels = document.querySelectorAll("#slider label");
    const prevIndex =
      FeaturesState[0] === labels.length - 3 ? 2 : FeaturesState[0] - 1;
    FeaturesState[1](prevIndex);
  };
  const check = (index) => setSelectedIndex(index);

  // testimonial slider
  const TestimonialState = React.useState(1);
  const selectedTestimonialIndex = TestimonialState[0];
  const setSelectedTestimonialIndex = TestimonialState[1];

  const testimonialCheckNext = () => {
    const labels = document.querySelectorAll("#testimonialSlider label");
    const nextIndex =
      selectedTestimonialIndex === labels.length - 1
        ? 0
        : selectedTestimonialIndex + 1;
    setSelectedTestimonialIndex(nextIndex);

    if (i < testimonialArray.length - 1) {
      i++;
    } else {
      i = 0;
    }
  };

  const testimonialCheckPrev = () => {
    const labels = document.querySelectorAll("#testimonialSlider label");
    const prevIndex =
      selectedTestimonialIndex === labels.length - 3
        ? 2
        : selectedTestimonialIndex - 1;
    setSelectedTestimonialIndex(prevIndex);

    if (i > 0) {
      i--;
    } else {
      i = testimonialArray.length - 1;
    }
  };

  const testimonialCheck = (index) => setSelectedTestimonialIndex(index);

  const playVideo = (item) => {
    $("#video").trigger("play");    $("#audio").trigger("play");
  };
  const pauseVideo = () => {
    $("#video").trigger("pause");
    $("#audio").trigger("pause");
  };
  const checkIcon = () => {
    $(".play, .pause").toggle();
  };

  // ANIMATIONS

  const easing = [0.6, -0.05, 0.01, 0.99] ;
  const fadeInUp = {
    initial: {
      y: 60, 
      opacity: 0
    },
    animate: {
      y: 0,
      opacity: 1,
      transition: {
        duration: 0.6,
        ease: easing,
        delay: 0.3
      }
    }
  } ;

  const stagger = {
    animate: {
      transition: {
        staggerChildren: 0.1
      }
    }
  }

  return (
    <AnimatePresence exitBeforeEnter>
    <motion.div className="App"
      animate= {{ opacity: 1, y: 0}}
      initial= {{ opacity: 0, y: -100}}
      transition= {{ duration: .6}}
      exit={{ opacity: 0, y: 300}}
      
    >
      <Head>
        <title>SpeechCorp</title>
        <link rel="icon" href="./assets/favicon.jpg" />
        <link rel="stylesheet" href="css/style.css"></link>
        <link
          href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
        <script src="js/script.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
      </Head>

      <motion.main 
      initial='initial'
      animate='animate'>
        <section className="text-gray-700 body-font">
          <header className="shadow-2xl lg:px-16 px-6 bg-white flex flex-wrap items-center lg:py-0 py-2">
            <div className="flex-1 flex justify-between items-center">
              <Link href="/">
                <a href="#">
                  <img
                    alt="logo"
                    className="w-40 mb-2"
                    src="./assets/logo.png"
                  ></img>
                </a>
              </Link>
            </div>

            <label for="menu-toggle" className="pointer-cursor lg:hidden block">
              <svg
                className="fill-current text-gray-900"
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
              >
                <title className="pointer-cursor">MENU</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
              </svg>
            </label>
            <input className="hidden" type="checkbox" id="menu-toggle" />

            <div
              className="hidden lg:flex lg:items-center lg:w-auto w-full"
              id="menu"
            >
              <nav>
                <ul className="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Product
                    </a>
                  </li>
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Pricing
                    </a>
                  </li>
                  <li>
                    <Link href="/AboutUs">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        About Us
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/Contact">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        Contact
                      </a>
                    </Link>
                  </li>
                  <li>
                    <a
                      className="font-bold lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400 lg:mb-0 mb-2"
                      href="#"
                    >
                      Sign In
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </header>
        </section>
        {/* hero section */}
        <motion.section className="text-gray-700 xl:h-screen body-font" variants={fadeInUp}>
          <motion.div
          
          
          className="container mx-auto flex px-5 sm:py-64 mt-16 sm:mt-0 md:flex-row flex-col items-center">
            <div className="lg:flex-grow lg:w-1/2 md:w-1/2 flex flex-col md:items-start md:text-left sm:mb-16 md:mb-0 items-center text-center">
              <h1 className="title-font sm:text-4xl text-3xl mb-4 font-bold text-gray-900">
                <span>Turning user's voice to actions</span>
                {/* 
                <span>Background Noise Hassle Free</span> */}
              </h1>
              <br className="hidden lg:inline-block" />
              <h1 className="title-font text-xl font-medium text-gray-500 mb-3">
                <span className="text-gray-600">Creating voice experiences</span> for your customers which are{" "}
                <br className="hidden lg:inline-block" /> powerful and intutive
                to use.
              </h1>
            </div>
            <div className="lg:w-1/2 md:w-1/2 flex justify-center">
              <img
                alt="demo"
                className="lg:w-auto w-96 lg:h-auto shadow-xl"
                src="./assets/hero.gif"
              />
              {/* <div
                id="audio_home"
                className="flex flex-wrap items-center justify-center"
              >
                <div className="flex audio-bar-sm">
                  <img
                    onClick={() => {
                      playVideo();
                      checkIcon();
                    }}
                    alt="logo"
                    className="pl-pa-btn play w-30 h-10 m-auto"
                    src="./assets/audio_header/play.svg"
                  ></img>
                  <img
                    onClick={() => {
                      pauseVideo();
                      checkIcon();
                    }}
                    alt="logo"
                    className="pl-pa-btn hidden pause w-30 h-10 m-auto"
                    src="./assets/audio_header/pause.svg"
                  ></img>
                  <video id="video" loop playsInline>
                    <source src="./assets/audio_header/audio.mp4" type="video/mp4" />
                  </video>
                  <audio id="audio" className="hidden" controls>
                    <source src="./assets/audio_header/song.mp3" />
                  </audio>
                  <img
                    alt="logo"
                    className="vol-btn w-30 h-10 m-auto"
                    src="speaker.svg"
                  ></img>
                </div>
                <div
                  id="audio_footer"
                  className="border-solid border-2 border-gray-500 bg-gray-100 p-4 rounded-md text-black-200 font-medium"
                >
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut ero labore et dolore
                </div>
              </div> */}
            </div>
          </motion.div>
        {/* help button */}

        <motion.section variants={fadeInUp} className="text-gray-700 body-font">
          <div className="container px-5 py-10 mx-auto">
            <div className="lg:w-full justify-center flex-col sm:flex-row sm:items-center col-reverse pr-0 mx-auto">
            <Link href="/Contact">
              <button className="flex-shrink-0 w-64 h-12 text-white bg-blue-500 border-0 py-2 px-8 shadow-xl focus:outline-none hover:bg-blue-600 rounded text-lg mt-0 sm:mt-0">
                Contact Now
              </button>
              </Link>
            </div>
          </div>
        </motion.section>
      </motion.section>


        <br></br>
        <br></br>

        {/* FEATURE */}

        <section className="text-gray-700 xl:h-screen body-font lg:mb-20" >
          <div className="text-center mb-5">
            <h1 className="sm:text-3xl text-2xl font-medium title-font text-gray-900 mb-4">
              Features
            </h1>
            <div className="w-16 h-1 rounded-full bg-blue-500 inline-flex"></div>
          </div>

          <br></br>
          <br></br>
          <motion.div className="flex sm:flex-none flex-wrap mt-10 lg:justify-center" 
            animate={{opacity: show, y: Yposition}}
            // initial= {{y: Yposition}}
            transition= {{ duration: .4}}
          >
            <div className="hidden lg:w-1/4 py-48 mr-0 md:mb-0 mb-6 md:flex md:flex-col text-center justify-center items-center ">
              <div className="w-20 h-20 hidden md:inline-flex items-center justify-center mb-5 flex-shrink-0">
                <button className="outline-none" onClick={checkPrev}>
                  <img
                    alt="back-icon"
                    className="prev w-30 h-10 mb-1"
                    src="./assets/back.svg"
                  ></img>
                </button>
              </div>
            </div>
            <div className="lg:w-2/4 md:mb-0 mb-6 flex flex-col text-center jusitfy-center items-center">
              <Swipeable onSwipeLeft={checkNext} onSwipeRight={checkPrev}>
                <section
                  id="slider"
                  className="w-16 h-20 inline-flex items-center justify-center mb-5 flex-shrink-0"
                >
                  <input
                    type="radio"
                    name="slider"
                    id="s1"
                    checked={selectedIndex === 0}
                    onClick={() => check(0)}
                  />
                  <input
                    type="radio"
                    name="slider"
                    id="s2"
                    checked={selectedIndex === 1}
                    onClick={() => check(1)}
                  />
                  <input
                    type="radio"
                    name="slider"
                    id="s3"
                    checked={selectedIndex === 2}
                    onClick={() => check(2)}
                  />
                  <motion.label className="flex flex-col justify-center" htmlFor="s1" id="slide1"
                  exit={{ opacity: 0}}>
                    <br />
                    <img
                      className="mx-auto h-40 w-40 md:h-64 md:w-64"
                      src="./assets/features/Intent Identification.png"
                    />
                    <br />
                    <h2 className="text-gray-900 px-2 md:px-0 text-lg title-font font-medium mb-3">
                      Accurate Intent Identification
                    </h2>
                    <br className="hidden md:block" />
                    <p className="leading-none md:leading-relaxed text-base px-12 md:px-16">
                      Lorem ipsum dolor sit amet,consectetur adipisicing
                      elit,sed do eiusmod tempor incididunt ut ero labore et
                      dolore.
                    </p>
                    <div className="xl:mt-16">
                      {/* <div className="w-32 md:w-64 h-1 rounded-full bg-gray-500 inline-flex"></div> */}
                      <br />
                      <hr className="line-style"/>
                      <Link href={{ pathname: "/Features", query: { name: 1}}}   >
                        <a
                          href="#1"
                          className="text-blue-600 inline-flex items-center"
                        >
                          Read More
                        </a>
                      </Link>
                    </div>
                  </motion.label>
                  <motion.label className="flex flex-col justify-center" htmlFor="s2" id="slide2"
                  exit={{ opacity: 0}}>
                    <br />
                    <img
                      className="mx-auto h-40 w-40 md:h-64 md:w-64"
                      src="./assets/features/On Device.png"
                    />
                    <br />
                    <h2 className="text-gray-900 px-2 md:px-0 text-lg title-font font-medium mb-3">
                      On device Voice Commands
                    </h2>
                    <br className="hidden md:block" />
                    <p className="leading-none md:leading-relaxed text-base px-12 md:px-16">
                      Lorem ipsum dolor sit amet,consectetur adipisicing
                      elit,sed do eiusmod tempor incididunt ut ero labore et
                      dolore.
                    </p>
                    <div className="xl:mt-16">
                      {/* <div className="w-32 md:w-64 h-1 rounded-full bg-gray-500 inline-flex"></div> */}
                      <br />
                      <hr className="line-style"/>
                      <Link href={{ pathname: "/Features", query: { name: 2}}}   >
                        <a
                          href="#feature-2"
                          className="text-blue-600 inline-flex items-center"
                        >
                          Read More
                        </a>
                      </Link>
                    </div>
                  </motion.label>
                  <motion.label className="flex flex-col justify-center" htmlFor="s3" id="slide3"
                  exit={{ opacity: 0}}>
                    <br />
                    <img
                      className="mx-auto h-40 w-40 md:h-64 md:w-64"
                      src="./assets/features/Hands Free.png"
                    />
                    <br />
                    <h2 className="text-gray-900 px-2 md:px-0 text-lg title-font font-medium mb-3">
                      Hands free Facilities
                    </h2>
                    <br className="hidden md:block" />
                    <p className="leading-none md:leading-relaxed text-base px-12 md:px-16">
                      Lorem ipsum dolor sit amet,consectetur adipisicing
                      elit,sed do eiusmod tempor incididunt ut ero labore et
                      dolore.
                    </p>
                    <div className="xl:mt-16">
                      {/* <div className="w-32 md:w-64 h-1 rounded-full bg-gray-500 inline-flex"></div> */}
                      <br />
                      <hr className="line-style"/>
                      <Link href={{ pathname: "/Features", query: { name: 3}}}   >
                        <a
                          href="#1"
                          className="text-blue-600 inline-flex items-center"
                        >
                          Read More
                        </a>
                      </Link>
                    </div>
                  </motion.label>
                </section>
              </Swipeable>
            </div>
            <div className=" lg:w-1/4 py-48 md:mb-0 mb-6 flex flex-col text-center justify-center items-center ">
              <div className="w-20 h-20  inline-flex items-center justify-center mb-5 flex-shrink-0">
                <button className="outline-none" onClick={checkNext}>
                  <img
                    alt="next-icon"
                    className="next w-30 h-10 mb-1"
                    src="./assets/forward.svg"
                  ></img>
                </button>
              </div>
            </div>
          </motion.div>
        </section>

        {/* POTENTIAL USE CASES */}

        <section className="text-gray-700 xl:h-screen my-auto body-font">
          <div className="container px-5 sm:py-24 mx-auto">
            <div className="text-center mb-20">
              <h1 className="sm:text-3xl text-2xl font-medium title-font text-gray-900 mb-4">
                Potential use cases
              </h1>
              <div className="w-16 h-1 rounded-full bg-blue-500 inline-flex"></div>
            </div>
            <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-32 h-32 inline-flex items-center justify-center mb-5 flex-shrink-0">
                  <img
                    alt="feature_1"
                    className="w-40 h-30"
                    src="./assets/use_cases/vBot.png"
                  ></img>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    vBot
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#1"
                    className="mt-3 text-blue-600 inline-flex items-center"
                  >
                    Learn More
                  </a>
                </div>
              </div>
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-32 h-32 inline-flex items-center justify-center mb-5 flex-shrink-0">
                  <img
                    alt="feature_2"
                    className="w-40 h-30"
                    src="./assets/use_cases/Touchless.png"
                  ></img>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    Touchless Experience
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#2"
                    className="mt-3 text-blue-600 inline-flex items-center"
                  >
                    Learn More
                  </a>
                </div>
              </div>
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-32 h-32 inline-flex items-center justify-center mb-5 flex-shrink-0">
                  <img
                    alt="feature_3"
                    className="w-40 h-30"
                    src="./assets/use_cases/IOT.png"
                  ></img>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    IOT
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#3"
                    className="mt-3 text-blue-600 inline-flex items-center"
                  >
                    Learn More
                  </a>
                </div>
              </div>
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-32 h-32 inline-flex items-center justify-center mb-5 flex-shrink-0">
                  <img
                    alt="feature_4"
                    className="w-40 h-30"
                    src="./assets/use_cases/Automobiles.png"
                  ></img>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    Automobiles
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#4"
                    className="mt-3 text-blue-600 inline-flex items-center"
                  >
                    Learn More
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* sample */}

        {/* <section className="text-gray-700 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="flex flex-col text-center w-full mb-20">
              <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                NOISES WE CAN CANCEL
              </h1>
              <div className="w-16 h-1 m-auto rounded-full text-center bg-blue-500 inline-flex"></div>

            </div>
            <div className="flex flex-wrap -m-4">
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/600x360"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Shooting Stars
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/601x361"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      The Catalyzer
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/603x363"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      The 400 Blows
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/602x362"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Neptune
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/605x365"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Holden Caulfield
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/606x366"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Alper Kamu
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/602x362"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Neptune
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/605x365"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Holden Caulfield
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/606x366"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Alper Kamu
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section> */}

        {/* testimonial */}

        <section className="text-gray-700 xl:h-screen my-auto body-font lg:mt-20 xl:mt-0" >
        <div className="flex flex-col text-center w-full mb-20">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mt-10 md:mt-0 mb-4 text-gray-900">
            Our Clients Say ...
          </h1>
        <div className="w-16 h-1 m-auto rounded-full text-center bg-blue-500 inline-flex"></div>

          {/* <p className="lg:w-2/3 mx-auto leading-relaxed text-base">Whatever cardigan tote bag tumblr hexagon brooklyn asymmetrical gentrify, subway tile poke farm-to-table. Franzen you probably haven't heard of them man bun deep jianbing selfies heirloom.</p> */}
        </div>
        <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4 justify-center">
          <div className="md:w-1/4 py-56 md:mb-0 mb-6 flex flex-col text-center items-end sm:z-10">
            <div className="w-20 h-20 inline-flex items-center justify-center mb-5 flex-shrink-0">
              <button className="outline-none" onClick={testimonialCheckPrev}>
                <img
                  alt="back-icon"
                  className="next w-30 h-10 mb-1"
                  src="./assets/back.svg"
                ></img>
              </button>
            </div>
          </div>
          <div className="medium-check md:w-2/4 md:mb-0 mb-6 flex flex-col text-center items-center">
            <Swipeable
              onSwipeLeft={testimonialCheckNext}
              onSwipeRight={testimonialCheckPrev}
            >
              <section id="testimonialSlider">
                <input
                  type="radio"
                  name="testimonialSlider"
                  id="t1"
                  checked={selectedTestimonialIndex === 0}
                  onClick={() => testimonialCheck(0)}
                />
                <label
                  htmlFor="t1"
                  id="testimonialSlide1"
                  className="justify-center"
                >
                  <div class="w-auto sm:w-full lg:mb-0 p-4 mx-auto">
                    <div class="h-full -mt-20 text-center sm:mx-auto sm:w-auto">
                      <img
                        alt="testimonial"
                        className="w-16 h-16 mb-4 mt-6 sm:w-32 sm:h-32 sm:mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100"
                        src={testimonialArray[i].src}
                      />
                      <h2 className="text-gray-900 font-bold title-font tracking-wider text-xl mt-0 mb-10">
                        {testimonialArray[i].name}
                      </h2>
                      <p className="leading-relaxed text-gray-600">
                        {testimonialArray[i].text}
                      </p>
                    </div>
                  </div>
                </label>

                <input
                  type="radio"
                  name="testimonialSlider"
                  id="t2"
                  checked={selectedTestimonialIndex === 1}
                  onClick={() => testimonialCheck(1)}
                />
                <label
                  htmlFor="t2"
                  id="testimonialSlide2"
                  className="justify-center"
                >
                  <div className="w-auto sm:w-full lg:mb-0 p-4 mx-auto">
                    <div className="h-full -mt-20 text-center sm:mx-auto sm:w-auto">
                      <img
                        alt="testimonial"
                        className="w-16 h-16 mb-4 mt-6 sm:w-32 sm:h-32 sm:mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100"
                        src={testimonialArray[i].src}
                      />
                      <h2 className="text-gray-900 font-bold title-font tracking-wider text-xl mt-0 mb-10">
                        {testimonialArray[i].name}
                      </h2>
                      <p className="leading-relaxed text-gray-600">
                        {testimonialArray[i].text}
                      </p>
                    </div>
                  </div>
                </label>

                <input
                  type="radio"
                  name="testimonialSlider"
                  id="t3"
                  checked={selectedTestimonialIndex === 2}
                  onClick={() => testimonialCheck(2)}
                />
                <label
                  htmlFor="t3"
                  id="testimonialSlide3"
                  className="justify-center"
                >
                  <div className="w-auto sm:w-full lg:mb-0 p-4 mx-auto">
                    <div className="h-full -mt-20 text-center sm:mx-auto sm:w-auto">
                      <img
                        alt="testimonial"
                        className="w-16 h-16 mb-4 mt-6 sm:w-32 sm:h-32 sm:mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100"
                        src={testimonialArray[i].src}
                      />
                      <h2 className="text-gray-900 font-bold title-font tracking-wider text-xl mt-0 mb-10">
                        {testimonialArray[i].name}
                      </h2>
                      <p className="leading-relaxed text-gray-600">
                        {testimonialArray[i].text}
                      </p>
                    </div>
                  </div>
                </label>
              </section>
            </Swipeable>
          </div>
          <div className="md:w-1/4 py-56 md:mb-0 mb-6 flex flex-col text-center items-start sm:z-10">
            <div className="w-20 h-20 inline-flex items-center justify-center mb-5 flex-shrink-0">
              <button className="outline-none" onClick={testimonialCheckNext}>
                <img
                  alt="next-icon"
                  className="next w-30 h-10 mb-1"
                  src="./assets/forward.svg"
                ></img>
              </button>
            </div>
          </div>
        </div>
        </section>
        {/* footer */}

        <footer className="text-gray-700 body-font">
          <div className="container px-5 py-8 mb-0 mx-auto flex items-center sm:flex-row flex-col">
            <img
              alt="logo"
              className="w-40 h-8 mb-8"
              src="./assets/logo_light.png"
            ></img>
            {/* <span className="ml-3 mb-6 text-xl sm:ml-4 sm:pl-4 sm:border-l-2 sm:border-gray-200">
              SpeechCorp.
            </span> */}
          </div>
          <div className="container px-5 py-12 pt-0 mx-auto">
            <div className="flex flex-wrap md:text-left text-center -mb-10 -mx-4">
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-bold text-gray-100 tracking-widest text-sm mb-3">
                  APP
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a href="#"
                      className="text-gray-100 hover:text-gray-100"
                    >
                      Product
                    </a>
                  </li>
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">Pricing</a>
                  </li>
                  {/* <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">
                      Download
                    </a>
                  </li> */}
                </nav>
              </div>
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-bold text-gray-100 tracking-widest text-sm mb-3">
                  ABOUT US
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">Team</a>
                  </li>
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">Careers</a>
                  </li>
                  {/* <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">
                      Sponsors
                    </a>
                  </li> */}
                </nav>
              </div>
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-bold text-gray-100 tracking-widest text-sm mb-3">
                  SUPPORT
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">Contact</a>
                  </li>
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">
                      Email Us
                    </a>
                  </li>
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">
                      Report a bug
                    </a>
                  </li>
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">FAQ</a>
                  </li>
                </nav>
              </div>
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-bold text-gray-100 tracking-widest text-sm mb-3">
                  LEGAL
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">
                      Privacy policies
                    </a>
                  </li>
                  <li>
                    <a href="#" className="text-gray-100 hover:text-gray-100">
                      Terms and conditions
                    </a>
                  </li>
                </nav>
              </div>
              <div className="lg:w-1/5 text-left w-full px-4">
                <h2 className="title-font font-bold text-gray-100 tracking-widest text-sm mb-3">
                  SOCIAL
                </h2>
                <span className="inline-flex lg:ml-auto lg:mt-0 mt-6 w-full justify-center md:justify-start md:w-auto">
                  <a href="#" className="text-gray-100">
                    <svg
                      fill="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                    </svg>
                  </a>
                  <a href="#" className="ml-3 text-gray-100">
                    <svg
                      fill="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
                    </svg>
                  </a>
                  <a href="#" className="ml-3 text-gray-100">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <rect
                        width="20"
                        height="20"
                        x="2"
                        y="2"
                        rx="5"
                        ry="5"
                      ></rect>
                      <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                    </svg>
                  </a>
                  <a href="#" className="ml-3 text-gray-100">
                    <svg
                      fill="currentColor"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="0"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path
                        stroke="none"
                        d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z"
                      ></path>
                      <circle cx="4" cy="4" r="2" stroke="none"></circle>
                    </svg>
                  </a>
                </span>
              </div>
            </div>
          </div>
          <div>
            <div className="container mx-auto py-4 px-5 flex flex-wrap flex-col items-center justify-center sm:flex-row">
              <p className="text-gray-100 text-sm text-center sm:text-left">
                © 2020 Speech Corp —
                <a
                  href="https://twitter.com/knyttneve"
                  className="text-gray-100 ml-1"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  All Rights Reserved.
                </a>
              </p>
              {/* <span className="sm:ml-auto sm:mt-0 mt-2 sm:w-auto w-full sm:text-left text-center text-gray-500 text-sm">
                Address -
              </span> */}
            </div>
          </div>
        </footer>
      </motion.main>
    </motion.div>
    </AnimatePresence>
  );
}
