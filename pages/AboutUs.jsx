import Head from "next/head";
import Link from "next/link";
import { motion } from "framer-motion";

export default function Contact() {
  // ANIMATIONS

  const easing = [0.6, -0.05, 0.01, 0.99] ;
  const fadeInUp = {
    initial: {
      y: 60, 
      opacity: 0
    },
    animate: {
      y: 0,
      opacity: 1,
      transition: {
        duration: 0.6,
        ease: easing,
        delay: 0.3
      }
    }
  } ;

  return (
    <motion.div className="App"
      animate= {{ opacity: 1, y: 0}}
      initial= {{ opacity: 0, y: -100}}
      transition= {{ duration: .6}}
      exit={{ opacity: 0, y: 300}}
    >
      <Head>
        <link
          href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
        <link rel="icon" href="../assets/favicon.jpg" />
        <link rel="stylesheet" href="css/style.css"></link>
      </Head>
      <motion.main 
      initial='initial'
      animate='animate'>
        <section className="text-gray-700 body-font">
          <header className="shadow-2xl lg:px-16 px-6 bg-white flex flex-wrap items-center lg:py-0 py-2">
            <div className="flex-1 flex justify-between items-center">
              <Link href="/">
                <a href="#">
                  <img
                    alt="logo"
                    className="w-40 mb-2"
                    src="./assets/logo.png"
                  ></img>
                </a>
              </Link>
            </div>

            <label for="menu-toggle" className="pointer-cursor lg:hidden block">
              <svg
                className="fill-current text-gray-900"
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
              >
                <title className="pointer-cursor">MENU</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
              </svg>
            </label>
            <input className="hidden" type="checkbox" id="menu-toggle" />

            <div
              className="hidden lg:flex lg:items-center lg:w-auto w-full"
              id="menu"
            >
              <nav>
                <ul className="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Product
                    </a>
                  </li>
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Pricing
                    </a>
                  </li>
                  <li>
                    <Link href="/AboutUs">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        About Us
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/Contact">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        Contact
                      </a>
                    </Link>
                  </li>
                  <li>
                    <a
                      className="font-bold lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400 lg:mb-0 mb-2"
                      href="#"
                    >
                      Sign In
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </header>
        </section>
        <motion.section variants={fadeInUp} className="about-back text-gray-100 bg-gray-700 body-font">
          <motion.div variants={fadeInUp} className="container px-5 py-24 mx-auto">
            <div className="flex flex-wrap w-full xl:w-1/2 mb-20">
              <div className="mb-6 lg:mb-0">
                <motion.h1  className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-100 flow-root">
                  About Us
                </motion.h1>
                <div className="h-1 w-32 bg-gray-100 rounded flow-root"></div>
              </div>
              <motion.p variants={fadeInUp} className="w-full mt-6 leading-relaxed block text-base text-left">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut ero labore et dolore.Lorem ipsum
                dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut ero labore et dolore.Lorem ipsum dolor sit
                amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut ero labore et dolore.
              </motion.p>
            </div>
          </motion.div>
        </motion.section>

        <motion.section variants={fadeInUp} className="text-gray-700 body-font">
          <motion.div variants={fadeInUp} className="container mx-auto flex px-5 py-10 md:py-24 md:flex-row flex-col items-center">
            <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
              <img
                className="object-cover object-center rounded"
                alt="hero"
                src="https://dummyimage.com/720x600"
              />
            </div>
            <div className="lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
              <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
                What We Offer ??
              </h1>
              <p className="mb-8 leading-relaxed">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut ero labore et dolore.Lorem ipsum
                dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut ero labore et dolore. Lorem ipsum dolor sit
                amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut ero labore et dolore.Lorem ipsum dolor sit amet,
                consectetur adipisicing elit, sed do eiusmod tempor incididunt
                ut ero labore et dolore. Lorem ipsum dolor sit amet, consectetur
                adipisicing elit, sed do eiusmod tempor incididunt
              </p>
              {/* <div className="flex justify-center">
        <button className="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
        <button className="ml-4 inline-flex text-gray-700 bg-gray-200 border-0 py-2 px-6 focus:outline-none hover:bg-gray-300 rounded text-lg">Button</button>
      </div> */}
            </div>
          </motion.div>
        </motion.section>

        <motion.section variants={fadeInUp} className="text-gray-700 body-font">
          <div className="container mx-auto flex px-5 py-10 md:py-24 md:flex-row flex-col items-center">
            <div className="block md:hidden lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10">
              <img
                className="object-cover object-center rounded"
                alt="hero"
                src="https://dummyimage.com/720x600"
              />
            </div>
            <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
              <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
                What makes us Different ??
              </h1>
              <p className="mb-8 leading-relaxed">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut ero labore et dolore.Lorem ipsum
                dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut ero labore et dolore. Lorem ipsum dolor sit
                amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut ero labore et dolore.Lorem ipsum dolor sit amet,
                consectetur adipisicing elit, sed do eiusmod tempor incididunt
                ut ero labore et dolore. Lorem ipsum dolor sit amet, consectetur
                adipisicing elit, sed do eiusmod tempor incididunt ut ero labore
                et dolore.Lorem ipsum dolor sit amet, consectetur
              </p>
              {/* <div className="flex justify-center">
        <button className="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
        <button className="ml-4 inline-flex text-gray-700 bg-gray-200 border-0 py-2 px-6 focus:outline-none hover:bg-gray-300 rounded text-lg">Button</button>
      </div> */}
            </div>
            <div className="hidden md:block lg:max-w-lg lg:w-full md:w-1/2 w-5/6">
              <img
                className="object-cover object-center rounded"
                alt="hero"
                src="https://dummyimage.com/720x600"
              />
            </div>
          </div>
        </motion.section>

        <motion.section variants={fadeInUp} className="about-back text-gray-700 bg-gray-500 body-font">
          <div className="container px-5 sm:py-24 mx-auto">
            <div className="text-center mb-20">
              <h1 className="sm:text-3xl text-2xl font-medium title-font text-gray-100 mb-4">
                What we actually do?
              </h1>
              <div className="w-24 h-1 rounded-full bg-gray-400 inline-flex"></div>
            </div>
            <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
              <div className="p-4 w-full md:w-1/3 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-32 h-32 inline-flex items-center justify-center rounded-full bg-blue-100 text-blue-500 mb-5 flex-shrink-0">
                  <img
                    alt="feature_1"
                    className="w-20 h-20"
                    src="./assets/layout.png"
                  ></img>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-100 text-lg title-font font-medium mb-3">
                    Lorem Ipsum
                  </h2>
                </div>
              </div>
              <br />
              <div className="p-4 w-full md:w-1/3 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-32 h-32 inline-flex items-center justify-center rounded-full bg-blue-100 text-blue-500 mb-5 flex-shrink-0">
                  <img
                    alt="feature_2"
                    className="w-20 h-20"
                    src="./assets/layout.png"
                  ></img>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-100 text-lg title-font font-medium mb-3">
                    Lorem Ipsum
                  </h2>
                </div>
              </div>
              <div className="p-4 w-full md:w-1/3 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-32 h-32 inline-flex items-center justify-center rounded-full bg-blue-100 text-blue-500 mb-5 flex-shrink-0">
                  <img
                    alt="feature_3"
                    className="w-20 h-20"
                    src="./assets/layout.png"
                  ></img>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-100 text-lg title-font font-medium mb-3">
                    Lorem Ipsum
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </motion.section>
        <motion.section variants={fadeInUp} className="text-gray-700 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="lg:w-2/3 flex flex-col sm:flex-row sm:items-center text-center mx-auto">
              <h1 className="flex-grow sm:pr-32 text-2xl font-medium title-font text-gray-900">
                Connect with us!
              </h1>
              <Link href="/Contact">
                <a className="flex-shrink-0 text-white bg-blue-500 border-0 py-2 px-20 focus:outline-none hover:bg-blue-600 rounded text-lg sm:mr-10 mt-10 sm:mt-0">
                  Contact now
                </a>
              </Link>
            </div>
          </div>
        </motion.section>
      </motion.main>
    </motion.div>
  );
}
