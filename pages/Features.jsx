import Head from "next/head";
import Link from "next/link";
import { motion } from 'framer-motion' ;
import React from 'react' ;
import { useRouter } from 'next/router' ;

export default function Features() {
  const router = useRouter() ;
  const [ feature1X, setFeature1X ] = React.useState(-1000)
  const [ feature2X, setFeature2X ] = React.useState(0)
  const [ feature3X, setFeature3X ] = React.useState(1000)

  // const feature1clicked = () => {
  //   setFeature1X('100%') ;
  //   setFeature2X(1800) ;
  //   setFeature3X(1800) ;
  // }

  // const feature2clicked = () => {
  //   setFeature1X(-1800) ;
  //   setFeature2X(0) ;
  //   setFeature3X(1800) ;
  // }

  // const feature3clicked = () => {
  //   setFeature1X(-1800) ;
  //   setFeature2X(-1800) ;
  //   setFeature3X('-100%') ;
  // }

  const name = router.query.name ;
  React.useEffect(() => {
      if(name){
        
        if(name == 1) {
        document.getElementById('feature1').click() ;
        // feature1clicked() ;
        // setFeature1X('100%') ;
        // setFeature2X(1800) ;
        // setFeature3X(1800) ;
      }
      else if(name == 2) {
        // feature2clicked() ;
        document.getElementById('feature2').click() ;
        // setFeature1X(-1800) ;
        // setFeature2X(0) ;
        // setFeature3X(1800) ;
      }
      else if( name == 3) {
        // feature3clicked() ;
        document.getElementById('feature3').click() ;
        // setFeature1X(-1800) ;
        // setFeature2X(-1800) ;
        // setFeature3X('-100%') ;
      }
      }
      
  }, [name]);

  
   // ANIMATIONS

   const easing = [0.6, -0.05, 0.01, 0.99] ;
   const fadeInUp = {
     initial: {
       y: 60, 
       opacity: 0
     },
     animate: {
       y: 0,
       opacity: 1,
       transition: {
         duration: 0.6,
         ease: easing,
         delay: 0.3
       }
     }
   } ;
 
   const stagger = {
     animate: {
       transition: {
         staggerChildren: 0.1
       }
     }
   }
 
  return (
    <motion.div
      className="App"
      animate= {{ opacity: 1, y: 0}}
      initial= {{ opacity: 0, y: -100}}
      transition= {{ duration: .6}}
      exit={{ opacity: 0, y: 300}}
    >
      <Head>
        <link
          href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
        <link rel="icon" href="../assets/favicon.jpg" />
        <link rel="stylesheet" href="css/style.css"></link>

      </Head>
      <motion.main 
      initial='initial'
      animate='animate'>
        <section className="text-gray-700 body-font">
          <header className="shadow-2xl lg:px-16 px-6 bg-white flex flex-wrap items-center lg:py-0 py-2">
            <div className="flex-1 flex justify-between items-center">
              <Link href="/">
              <a href="#">
                <img
                  alt="logo"
                  className="w-40 mb-2"
                  src="./assets/logo.png"
                ></img>
              </a>
              </Link>
            </div>

            <label for="menu-toggle" className="pointer-cursor lg:hidden block">
              <svg
                className="fill-current text-gray-900"
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
              >
                <title className="pointer-cursor">MENU</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
              </svg>
            </label>
            <input className="hidden" type="checkbox" id="menu-toggle" />

            <div
              className="hidden lg:flex lg:items-center lg:w-auto w-full"
              id="menu"
            >
              <nav>
                <ul className="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
                  <li>
                    <Link href="/AboutUs">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        About Us
                      </a>
                    </Link>
                  </li>
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Product
                    </a>
                  </li>
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Pricing
                    </a>
                  </li>
                  <li>
                    <Link href="/Contact">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        Contact
                      </a>
                    </Link>
                  </li>
                  <li>
                    <a
                      className="font-bold lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400 lg:mb-0 mb-2"
                      href="#"
                    >
                      Sign In
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </header>
        </section>


        <motion.div variants={fadeInUp} className="text-center py-10 mt-4 flex justify-around text-center items-center">
          <h1 onClick={() => {
            // feature1clicked()
            setFeature1X('100%') ;
            setFeature2X(1800) ;
            setFeature3X(1800) ;
            document.getElementById('feature1').style.color = '#4299e1' ;
            document.getElementById('feature2').style.color = '#1a202c' ;
            document.getElementById('feature3').style.color = '#1a202c' ;
          }} className="sm:text-3xl text-xl font-medium title-font text-gray-900 mb-0" id='feature1'>
            <button className="font-medium focus:outline-none">Feature 1</button>
          </h1>

          <h1 onClick={() => {
            // feature2clicked()
            setFeature1X(-1800) ;
            setFeature2X(0) ;
            setFeature3X(1800) ;
            document.getElementById('feature1').style.color = '#1a202c' ;
            document.getElementById('feature2').style.color = '#4299e1' ;
            document.getElementById('feature3').style.color = '#1a202c' ;
            }} className="sm:text-3xl text-xl font-medium title-font text-gray-900 mb-0" id='feature2'>
            <button  className="font-medium focus:outline-none">Feature 2</button>
          </h1>

          <h1 onClick={() => {
            // feature3clicked()
            setFeature1X(-1800) ;
            setFeature2X(-1800) ;
            setFeature3X('-100%') ;
            document.getElementById('feature1').style.color = '#1a202c';
            document.getElementById('feature2').style.color = '#1a202c' ;
            document.getElementById('feature3').style.color = '#4299e1' ;
            }} className="sm:text-3xl text-xl font-medium title-font text-gray-900 mb-0" id='feature3'>
            <button className="font-medium focus:outline-none">Feature 3</button>
          </h1>
          
        </motion.div>
        {/* <motion.div
        animate= {{ x: stripX}}
        transition= {{ duration: 1}}
        className="w-20 h-1 rounded-full bg-blue-600 inline-flex md-hidden"></motion.div> */}
        
        {/* <motion.div
        animate= {{ x: stripX}}
        transition= {{ duration: 1}}
        className="w-20 h-1 rounded-full bg-blue-600 hidden md-inline-flex lg-hidden"></motion.div>

        <motion.div
        animate= {{ x: stripX}}
        transition= {{ duration: 1}}
        className="w-20 h-1 rounded-full bg-blue-600 hidden lg:inline-flex"></motion.div> */}

        <div variants={fadeInUp} className="flex justify-evenly w-screen h-screen items-center">
        <motion.section
          animate= {{ x: feature1X }}
          transition= {{ duration: 1}}
        class="text-gray-700 body-font w-screen h-screen">
          <motion.div stagger={stagger} class="container mx-auto flex px-5 py-10 md:py-24 md:flex-row flex-col items-center">
            <div class="w-screen mb-10 md:mb-0">
              <motion.img variants={fadeInUp}
                class="object-cover object-center rounded"
                alt="hero"
                src="https://dummyimage.com/720x600"
              />
            </div>
            <div class="lg:flex-grow w-screen lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
              <motion.h1 variants={fadeInUp} class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
                Feature 1 - Lorem Ipsum
              </motion.h1>
              <motion.p variants={fadeInUp} class="mb-8 leading-relaxed">
                <b>Lorem ipsum</b> dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quiepakis nostrud exercitation
                ullamco
              </motion.p>
              <motion.p variants={fadeInUp} class="mb-8 leading-relaxed">
                <b>laboris nsi ut aliquip</b> ex ea comepmodo consetquat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse
                cfgillum dolore eutpe fugiat nulla pariatur. Excepteur sint
                occaecat cupidatat non proident, sunt inpeku culpa.
              </motion.p>
              {/* <div class="flex justify-center">
        <button class="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
        <button class="ml-4 inline-flex text-gray-700 bg-gray-200 border-0 py-2 px-6 focus:outline-none hover:bg-gray-300 rounded text-lg">Button</button>
        </div> */}
            </div>
          </motion.div>
        </motion.section>

        <motion.section
          animate= {{ x: feature2X }}
          transition= {{ duration: 1}}
          id="feature-2" class="text-gray-700 body-font w-screen h-screen">
          <motion.div stagger={stagger} class="container mx-auto flex px-5 py-10 md:py-24 md:flex-row flex-col items-center">
          <div class="w-screen mb-10 md:mb-0">
              <motion.img variants={fadeInUp}
                class="object-cover object-center rounded"
                alt="hero"
                src="https://dummyimage.com/720x600"
              />
            </div>
            <div class="lg:flex-grow w-screen lg:pr-24 md:pl-16 flex flex-col md:items-start md:text-left  md:mb-0 items-center text-center">
              <motion.h1 variants={fadeInUp} class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
                Feature 2 - Lorem Ipsum
              </motion.h1>
              <motion.p variants={fadeInUp} class="mb-8 leading-relaxed">
                <b>Lorem ipsum</b> dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quiepakis nostrud exercitation
                ullamco
              </motion.p>
              <motion.p variants={fadeInUp} class="mb-8 leading-relaxed">
                <b>laboris nsi ut aliquip</b> ex ea comepmodo consetquat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse
                cfgillum dolore eutpe fugiat nulla pariatur. Excepteur sint
                occaecat cupidatat non proident, sunt inpeku culpa.
              </motion.p>
              {/* <div class="flex justify-center">
        <button class="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
        <button class="ml-4 inline-flex text-gray-700 bg-gray-200 border-0 py-2 px-6 focus:outline-none hover:bg-gray-300 rounded text-lg">Button</button>
        </div> */}
            </div>
            
          </motion.div>
        </motion.section>

        <motion.section
          animate= {{ x: feature3X }}
          transition= {{ duration: 1}}
           class="text-gray-700 body-font w-screen h-screen">
          <motion.div stagger={stagger} class="container mx-auto flex px-5 py-10 md:py-24 md:flex-row flex-col items-center">
            <div class="w-screen mb-10 md:mb-0">
              <motion.img variants={fadeInUp}
                class="object-cover object-center rounded"
                alt="hero"
                src="https://dummyimage.com/720x600"
              />
            </div>
            <div class="lg:flex-grow w-screen lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
              <motion.h1 variants={fadeInUp} class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
                Feature 3 - Lorem Ipsum
              </motion.h1>
              <motion.p variants={fadeInUp} class="mb-8 leading-relaxed">
                <b>Lorem ipsum</b> dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quiepakis nostrud exercitation
                ullamco
              </motion.p>
              <motion.p variants={fadeInUp} class="mb-8 leading-relaxed">
                <b>laboris nsi ut aliquip</b> ex ea comepmodo consetquat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse
                cfgillum dolore eutpe fugiat nulla pariatur. Excepteur sint
                occaecat cupidatat non proident, sunt inpeku culpa.
              </motion.p>
              {/* <div class="flex justify-center">
        <button class="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
        <button class="ml-4 inline-flex text-gray-700 bg-gray-200 border-0 py-2 px-6 focus:outline-none hover:bg-gray-300 rounded text-lg">Button</button>
        </div> */}
            </div>
          </motion.div>
        </motion.section>
        </div>


      </motion.main>
    </motion.div>
  );
}
