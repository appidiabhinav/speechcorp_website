import Head from "next/head";
import Link from "next/link";
import { motion } from "framer-motion";

export default function Feature1() {
  return (
    <motion.div
      className="App"
      animate={{ opacity: 1, y: 0 }}
      initial={{ opacity: 0, y: -100 }}
      transition={{ duration: 1 }}
    >
      <Head>
        <link
          href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
        <link rel="icon" href="../assets/favicon.jpg" />
        <link rel="stylesheet" href="../css/style.css"></link>
      </Head>
      <main>
        <section className="text-gray-700 body-font">
          <header className="shadow-2xl lg:px-16 px-6 bg-white flex flex-wrap items-center lg:py-0 py-2">
            <div className="flex-1 flex justify-between items-center">
              <Link href="/">
                <a href="#">
                  <img
                    alt="logo"
                    className="w-40 mb-2"
                    src="../assets/logo.png"
                  ></img>
                </a>
              </Link>
            </div>

            <label for="menu-toggle" className="pointer-cursor lg:hidden block">
              <svg
                className="fill-current text-gray-900"
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
              >
                <title className="pointer-cursor">MENU</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
              </svg>
            </label>
            <input className="hidden" type="checkbox" id="menu-toggle" />

            <div
              className="hidden lg:flex lg:items-center lg:w-auto w-full"
              id="menu"
            >
              <nav>
                <ul className="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
                  <li>
                    <Link href="/AboutUs">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        About Us
                      </a>
                    </Link>
                  </li>
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Product
                    </a>
                  </li>
                  <li>
                    <a
                      className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                      href="#"
                    >
                      Pricing
                    </a>
                  </li>
                  <li>
                    <Link href="/Contact">
                      <a
                        className="lg:p-4 lg:mr-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                        href="#"
                      >
                        Contact
                      </a>
                    </Link>
                  </li>
                  <li>
                    <a
                      className="font-bold lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400 lg:mb-0 mb-2"
                      href="#"
                    >
                      Sign In
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </header>
        </section>
        <div className="pt-10 flex flex-row justify-center">
          <div className="flex flex-row mx-auto">
            <svg class="svg-icon" viewBox="0 0 20 20">
              <path d="M11.739,13.962c-0.087,0.086-0.199,0.131-0.312,0.131c-0.112,0-0.226-0.045-0.312-0.131l-3.738-3.736c-0.173-0.173-0.173-0.454,0-0.626l3.559-3.562c0.173-0.175,0.454-0.173,0.626,0c0.173,0.172,0.173,0.451,0,0.624l-3.248,3.25l3.425,3.426C11.911,13.511,11.911,13.789,11.739,13.962 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.148,3.374,7.521,7.521,7.521C14.147,17.521,17.521,14.148,17.521,10"></path>
            </svg>
            <Link href="/">
              <a className="text-xs ml-1 font-medium text-blue-800">
              Back to Home
              </a>
            </Link>
          </div>
          <div className="flex flex-row mx-auto">
            {/* <svg class="svg-icon" viewBox="0 0 20 20">
              <path d="M12.522,10.4l-3.559,3.562c-0.172,0.173-0.451,0.176-0.625,0c-0.173-0.173-0.173-0.451,0-0.624l3.248-3.25L8.161,6.662c-0.173-0.173-0.173-0.452,0-0.624c0.172-0.175,0.451-0.175,0.624,0l3.738,3.736C12.695,9.947,12.695,10.228,12.522,10.4 M18.406,10c0,4.644-3.764,8.406-8.406,8.406c-4.644,0-8.406-3.763-8.406-8.406S5.356,1.594,10,1.594C14.643,1.594,18.406,5.356,18.406,10M17.521,10c0-4.148-3.374-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.147,17.521,17.521,14.147,17.521,10"></path>
            </svg>
            <Link href="/features/feature3">
              <a className="text-xs ml-1 font-medium text-blue-800">
                Feature 03
              </a>
            </Link> */}
          </div>
        </div>

        <section class="text-gray-700 body-font">
          <div class="container mx-auto flex px-5 py-10 md:py-24 md:flex-row flex-col items-center">
            <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0">
              <img
                class="object-cover object-center rounded"
                alt="hero"
                src="https://dummyimage.com/720x600"
              />
            </div>
            <div class="lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
              <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
                Feature 2 - Lorem Ipsum
              </h1>
              <p class="mb-8 leading-relaxed">
                <b>Lorem ipsum</b> dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quiepakis nostrud exercitation
                ullamco
              </p>
              <p class="mb-8 leading-relaxed">
                <b>laboris nsi ut aliquip</b> ex ea comepmodo consetquat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse
                cfgillum dolore eutpe fugiat nulla pariatur. Excepteur sint
                occaecat cupidatat non proident, sunt inpeku culpa.
              </p>
              {/* <div class="flex justify-center">
        <button class="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
        <button class="ml-4 inline-flex text-gray-700 bg-gray-200 border-0 py-2 px-6 focus:outline-none hover:bg-gray-300 rounded text-lg">Button</button>
      </div> */}
            </div>
          </div>
        </section>
      </main>
    </motion.div>
  );
}
