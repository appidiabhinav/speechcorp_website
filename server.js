const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()

  server.get('/AboutUs', (req, res) => {
    return app.render(req, res, '/AboutUs', req.query)
  })

  server.get('/Contact', (req, res) => {
    return app.render(req, res, '/Contact', req.query)
  })

  server.get('/features/feature1', (req, res) => {
    return app.render(req, res, '/features/feature1', req.query)
  })

  server.get('/features/feature2', (req, res) => {
    return app.render(req, res, '/features/feature2', req.query)
  })

  server.get('/features/feature3', (req, res) => {
    return app.render(req, res, '/features/feature3', req.query)
  })

  server.all('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
